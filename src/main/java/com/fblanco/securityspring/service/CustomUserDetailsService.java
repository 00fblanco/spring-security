package com.fblanco.securityspring.service;

import com.fblanco.securityspring.model.CustomUserDetails;
import com.fblanco.securityspring.model.User;
import com.fblanco.securityspring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService{

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
         Optional<User> userOptional = userRepository.findByName(username);

         userOptional
                 .orElseThrow(() -> new UsernameNotFoundException("Username nor found"));

         return userOptional
                 .map(CustomUserDetails::new).get();
    }
}
